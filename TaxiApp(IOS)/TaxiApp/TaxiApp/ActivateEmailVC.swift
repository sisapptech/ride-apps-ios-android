//
//  ActivateEmailVC.swift
//  TaxiApp
//
//  Created by Sohit on 05/05/17.
//  Copyright © 2017 Sohit. All rights reserved.
//

import UIKit

class ActivateEmailVC: UIViewController {

    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var emailBackImage: UIImageView!
    @IBOutlet var activationBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        emailBackImage.layer.cornerRadius = 5
        emailBackImage.layer.masksToBounds = true
        
        activationBtn.layer.cornerRadius = 5
        activationBtn.layer.masksToBounds = true
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func actionActivation(_ sender: UIButton) {
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
