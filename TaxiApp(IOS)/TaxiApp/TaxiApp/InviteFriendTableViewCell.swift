//
//  InviteFriendTableViewCell.swift
//  TaxiApp
//
//  Created by Sohit on 06/05/17.
//  Copyright © 2017 Sohit. All rights reserved.
//

import UIKit

class InviteFriendTableViewCell: UITableViewCell {

    @IBOutlet weak var nameCell: UILabel!
    @IBOutlet weak var inviteBtnCell: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
