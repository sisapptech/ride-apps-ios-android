//
//  InviteFriendVC.swift
//  TaxiApp
//
//  Created by Sohit on 06/05/17.
//  Copyright © 2017 Sohit. All rights reserved.
//

import UIKit

class InviteFriendVC: UIViewController, UITableViewDataSource, UITableViewDelegate

{
    let swiftBlogs = ["Ray Wenderlich", "NSHipster", "iOS Developer Tips", "Jameson Quave", "Natasha The Robot", "Coding Explorer", "That Thing In Swift", "Andrew Bancroft", "iAchieved.it", "Airspeed Velocity"]
    
    @IBOutlet weak var inviteFrdTableView: UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.inviteFrdTableView.delegate = self
        self.inviteFrdTableView.dataSource = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return swiftBlogs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // let cell = self.flagReportTableView.dequeueReusableCell(withIdentifier: textCellIdentifier, for: indexPath as IndexPath)
        
        let cell = self.inviteFrdTableView.dequeueReusableCell(withIdentifier: "InviteFrdCell", for: indexPath) as! InviteFriendTableViewCell
        
        let row = indexPath.row
        //cell.textLabel?.text = swiftBlogs[row]
        cell.nameCell.text = swiftBlogs[row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
        
    }

}
