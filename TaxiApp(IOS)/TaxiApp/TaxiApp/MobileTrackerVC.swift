//
//  MobileTrackerVC.swift
//  TaxiApp
//
//  Created by Sohit on 05/05/17.
//  Copyright © 2017 Sohit. All rights reserved.
//

import UIKit

class MobileTrackerVC: UIViewController {

    @IBOutlet var numberTextField: UITextField!
    @IBOutlet var numberBackImage: UIImageView!
    @IBOutlet var traceBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        traceBtn.layer.cornerRadius = 5
        traceBtn.layer.masksToBounds = true
        
        numberBackImage.layer.cornerRadius = 5
        numberBackImage.layer.masksToBounds = true

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func actionTrace(_ sender: UIButton) {
    }

}
