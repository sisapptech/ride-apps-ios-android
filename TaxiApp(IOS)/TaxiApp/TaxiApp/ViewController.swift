//
//  ViewController.swift
//  TaxiApp
//
//  Created by Sohit on 04/05/17.
//  Copyright © 2017 Sohit. All rights reserved.
//

import UIKit


class ViewController: UIViewController {
    @IBOutlet var logoHeight: NSLayoutConstraint!
    @IBOutlet var logoWidth: NSLayoutConstraint!
    @IBOutlet var logoTopConstant: NSLayoutConstraint!
    @IBOutlet var passwordCheckBoxConstant: NSLayoutConstraint!
    @IBOutlet var btnRemember: UIButton!
    @IBOutlet var emailPasswordConstant: NSLayoutConstraint!
    @IBOutlet var fbBottomConstant: NSLayoutConstraint!
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var passwordImgHeight: NSLayoutConstraint!
    @IBOutlet var emailTextField: UITextField!

    @IBOutlet var loginBtnHeight: NSLayoutConstraint!
    @IBOutlet var emailBackImage: UIImageView!
    @IBOutlet var passwordBackImage: UIImageView!
    @IBOutlet var loginBtn: UIButton!
    @IBOutlet var centerImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = true
        
        let bounds = UIScreen.main.bounds
        let width = bounds.size.width
    
        if width <= 320
        {
            passwordImgHeight.constant = 35
            loginBtnHeight.constant = 35
            emailPasswordConstant.constant = 15
            fbBottomConstant.constant = -15
            passwordCheckBoxConstant.constant = 10
            logoWidth.constant = 100
            logoHeight.constant = 100
            logoTopConstant.constant = 20

        }
        else
        {
            passwordImgHeight.constant = 45
            loginBtnHeight.constant = 45
            emailPasswordConstant.constant = 20
            fbBottomConstant.constant = -20
            passwordCheckBoxConstant.constant = 20
            logoWidth.constant = 120
            logoHeight.constant = 120
            logoTopConstant.constant = 30

        }
        
        centerImage.layer.cornerRadius = 10
        centerImage.layer.borderColor = UIColor.gray.cgColor
        centerImage.layer.borderWidth = 3
        centerImage.layer.masksToBounds = true
        
        emailBackImage.layer.cornerRadius = 5
        emailBackImage.layer.masksToBounds = true
        
        passwordBackImage.layer.cornerRadius = 5
        passwordBackImage.layer.masksToBounds = true
        
        loginBtn.layer.cornerRadius = 5
        loginBtn.layer.masksToBounds = true
        
        btnRemember.layer.cornerRadius = 5
        btnRemember.layer.masksToBounds = true


        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func actionLinkedin(_ sender: UIButton) {
    }

    @IBAction func actionRemember(_ sender: UIButton) {
    }
    @IBAction func actionFb(_ sender: UIButton) {
    }
    @IBAction func actionForgotPassword(_ sender: UIButton) {
    }
    @IBAction func actionTwitter(_ sender: UIButton) {
    }
    @IBAction func actionGooglePlus(_ sender: UIButton) {
    }

    @IBAction func actionLoginIn(_ sender: UIButton)
    {
        let mapViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "MyAccountVC") as? MyAccountVC
        self.navigationController?.pushViewController(mapViewControllerObj!, animated: true)
        
    }
}

