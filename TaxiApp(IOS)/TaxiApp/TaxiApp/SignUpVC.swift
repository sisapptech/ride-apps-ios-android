//
//  SignUpVC.swift
//  TaxiApp
//
//  Created by Sohit on 05/05/17.
//  Copyright © 2017 Sohit. All rights reserved.
//

import UIKit

class SignUpVC: UIViewController {
    @IBOutlet var signUpBtn: UIButton!

    @IBOutlet var userProfileImg: UIImageView!
    @IBOutlet var lastNameBackImage: UIImageView!
    @IBOutlet var enailBackImage: UIImageView!
    @IBOutlet var userSignBackImage: UIImageView!
    @IBOutlet var confirnPassText: UITextField!
    @IBOutlet var confirnPassTextField: UITextField!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var passTextField: UITextField!
    @IBOutlet var mobileBackImage: UIImageView!
    @IBOutlet var lastNameText: UITextField!
    @IBOutlet var firstNameText: UITextField!
    @IBOutlet var conformBackImage: UIImageView!
    @IBOutlet var passwordBackImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        signUpBtn.layer.cornerRadius = 5
        signUpBtn.layer.masksToBounds = true
        
        lastNameBackImage.layer.cornerRadius = 5
        lastNameBackImage.layer.masksToBounds = true

        
        enailBackImage.layer.cornerRadius = 5
        enailBackImage.layer.masksToBounds = true

        userSignBackImage.layer.cornerRadius = 5
        userSignBackImage.layer.masksToBounds = true

        mobileBackImage.layer.cornerRadius = 5
        mobileBackImage.layer.masksToBounds = true

        conformBackImage.layer.cornerRadius = 5
        conformBackImage.layer.masksToBounds = true

        passwordBackImage.layer.cornerRadius = 5
        passwordBackImage.layer.masksToBounds = true

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func actionOpenGallery(_ sender: UIButton) {
    }

    @IBAction func actionSignUp(_ sender: UIButton) {
    }
}
