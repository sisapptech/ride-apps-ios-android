//
//  SideMenuVC.swift
//  TaxiApp
//
//  Created by Sohit on 06/05/17.
//  Copyright © 2017 Sohit. All rights reserved.
//

import UIKit

class SideMenuVC: UIViewController , UITableViewDataSource, UITableViewDelegate
{
let swiftBlogs = ["Moterbike", "KeKe-TuKTuK", "Free Ride", "Stores & Shops", "Delivery", "Help", "Setting", "Logout"]
    
    @IBOutlet var userImageView: UIImageView!
    @IBOutlet var userNameLbl: UILabel!
    let swiftImagesBlogs = ["bike", "keke", "freeride", "stories", "delivery", "help", "setting", "logout"]
    
    var carBol : Bool! = nil
    
    @IBOutlet var inerTableViewBtns: UIView!
    @IBOutlet weak var carView: UIView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var carInerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var sideMenuTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    
        carView.frame.size.height = 50
        carInerViewHeight.constant = 0
        inerTableViewBtns.isHidden = true
        carBol = false
     
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func carButtonPressed(_ sender: Any) {
        if carBol == false {
//            carView.frame.size.height = 188
//            carInerViewHeight.constant = 137
            carView.frame.size.height = 153
            carInerViewHeight.constant = 110
            inerTableViewBtns.isHidden = false
            carBol = true
            sideMenuTableView.reloadData()


        }
        else{
            carView.frame.size.height = 50
            carInerViewHeight.constant = 0
            inerTableViewBtns.isHidden = true
            carBol = false
            sideMenuTableView.reloadData()

            
        }
        
    }
    @IBAction func normalButtonPressed(_ sender: Any) {
    }
   
    @IBAction func cruiseButtonClicked(_ sender: Any) {
    }
    @IBAction func executiveButtonClicked(_ sender: Any) {
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return swiftBlogs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // let cell = self.flagReportTableView.dequeueReusableCell(withIdentifier: textCellIdentifier, for: indexPath as IndexPath)
        
        let cell = self.sideMenuTableView.dequeueReusableCell(withIdentifier: "SideMenuCell", for: indexPath) as! SideMenuTableViewCell
        
        let row = indexPath.row
        //cell.textLabel?.text = swiftBlogs[row]
        cell.cellName.text = swiftBlogs[row]
        cell.cellImage?.image = UIImage(named: swiftImagesBlogs[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }

}
