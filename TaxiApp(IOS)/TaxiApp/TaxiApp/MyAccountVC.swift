//
//  MyAccountVC.swift
//  TaxiApp
//
//  Created by Sohit on 05/05/17.
//  Copyright © 2017 Sohit. All rights reserved.
//

import UIKit

class MyAccountVC: UIViewController {
    @IBOutlet var userName: UILabel!
    @IBOutlet var mobileTextField: UITextField!

    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var userImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionOpenSideMenu(_ sender: UIButton)
    {
        menuContainerViewController.toggleLeftSideMenuCompletion(nil)

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
