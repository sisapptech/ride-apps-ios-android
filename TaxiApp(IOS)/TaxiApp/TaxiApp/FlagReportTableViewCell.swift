//
//  FlagReportTableViewCell.swift
//  TaxiApp
//
//  Created by Sohit on 04/05/17.
//  Copyright © 2017 Sohit. All rights reserved.
//

import UIKit

class FlagReportTableViewCell: UITableViewCell {
    @IBOutlet weak var cellUserName: UILabel!
    @IBOutlet weak var cellUserImage: UIImageView!
    @IBOutlet weak var cellTime: UILabel!
    @IBOutlet weak var cellDescription: UILabel!

    @IBOutlet var cellNotyLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
