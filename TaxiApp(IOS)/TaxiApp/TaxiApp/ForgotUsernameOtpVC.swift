//
//  ForgotUsernameOtpVC.swift
//  TaxiApp
//
//  Created by Sohit on 07/05/17.
//  Copyright © 2017 Sohit. All rights reserved.
//

import UIKit

class ForgotUsernameOtpVC: UIViewController {

    @IBOutlet var cancelBtn: UIButton!
    @IBOutlet var verfyBtn: UIButton!
    @IBOutlet var innerBackImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        
        innerBackImage.layer.cornerRadius = 10
        innerBackImage.layer.masksToBounds = true
        
        verfyBtn.layer.cornerRadius = 5
        verfyBtn.layer.masksToBounds = true
        
        cancelBtn.layer.cornerRadius = 5
        cancelBtn.layer.masksToBounds = true

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
