//
//  ActivateMobileCodeVC.swift
//  TaxiApp
//
//  Created by Sohit on 06/05/17.
//  Copyright © 2017 Sohit. All rights reserved.
//

import UIKit

class ActivateMobileCodeVC: UIViewController {
    @IBOutlet var activateBtn: UIButton!

    @IBOutlet var codeTextField: UITextField!
    @IBOutlet var codeBackImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        activateBtn.layer.cornerRadius = 5
        activateBtn.layer.masksToBounds = true
        codeBackImage.layer.cornerRadius = 5
        codeBackImage.layer.masksToBounds = true
        // Do any additional setup after loading the view.
    }

    @IBAction func actionResendCode(_ sender: UIButton) {
    }
    @IBAction func actionActivation(_ sender: UIButton) {
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
