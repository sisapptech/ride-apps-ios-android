//
//  ImeiTrackerVC.swift
//  TaxiApp
//
//  Created by Sohit on 05/05/17.
//  Copyright © 2017 Sohit. All rights reserved.
//

import UIKit

class ImeiTrackerVC: UIViewController {
    @IBOutlet var fieldBackImage: UIImageView!
    @IBOutlet var imeiTextField: UITextField!

    @IBOutlet var traceBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        traceBtn.layer.cornerRadius = 5
        traceBtn.layer.masksToBounds = true
        
        fieldBackImage.layer.cornerRadius = 5
        fieldBackImage.layer.masksToBounds = true

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func actionTrack(_ sender: UIButton) {
    }

}
