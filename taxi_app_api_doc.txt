//basic auth cridentials
username = t@xi_app
password = taxi@123

1. driver register api
http://demo.quantumtechnosoft.in/app/api/driver_register
method:post
parameters: first_name,last_name,email,password,phone,image,latitude,longitude,reg_id

2. driver login api
http://demo.quantumtechnosoft.in/app/api/driver_login
method:post
parameters: email,password,reg_id,imei_no,latitude,longitude

3. get driver profile detail api
http://demo.quantumtechnosoft.in/app/api/get_driver_profile
method:post
parameters: driver_id

4.driver update profile api
http://demo.quantumtechnosoft.in/app/api/driver_update_profile
method:post
parameters: id,first_name,last_name,email,password,phone,image,latitude,longitude,reg_id

5. match otp verification api
http://demo.quantumtechnosoft.in/app/api/match_otp
method:post
parameters:id,otp

6. resend otp api
http://demo.quantumtechnosoft.in/app/api/resend_otp
method:post
parameters:login_id

